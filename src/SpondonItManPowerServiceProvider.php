<?php

namespace SpondonIt\ManPowerService;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use SpondonIt\ManPowerService\Middleware\ManPowerService;

class SpondonItManPowerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(ManPowerService::class);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'manpower');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'manpower');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
