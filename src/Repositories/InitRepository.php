<?php

namespace SpondonIt\ManPowerService\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Modules\Setting\Model\GeneralSetting;
use App\Models\Notification;
use Illuminate\Support\Facades\Cache;
use Modules\RolePermission\Entities\Role;

class InitRepository {

    public function init() {
        config([
            'app.item' => '5049919',
            'spondonit.module_manager_model' => \App\InfixModuleManager::class,
            'spondonit.module_manager_table' => 'infix_module_managers',

            'spondonit.settings_model' => \Modules\Setting\Model\GeneralSetting::class,
            'spondonit.module_model' => \Nwidart\Modules\Facades\Module::class,

            'spondonit.user_model' => \App\Models\User::class,
            'spondonit.settings_table' => 'general_settings',
            'spondonit.database_file' => '',
            'spondonit.setting_key_column' => 'name',
            'spondonit.settings_value_column' => 'value',
            'spondonit.support_multi_connection' => false
        ]);
    }

    public function config()
    {
        Builder::macro('whereLike', function ($attributes, string $searchTerm) {
            $this->where(function (Builder $query) use ($attributes, $searchTerm) {
                foreach (Arr::wrap($attributes) as $attribute) {
                    $query->when(
                        Str::contains($attribute, '.'),
                        function (Builder $query) use ($attribute, $searchTerm) {
                            [$relationName, $relationAttribute] = explode('.', $attribute);
                            $query->orWhereHas($relationName, function (Builder $query) use ($relationAttribute, $searchTerm) {
                                $query->where($relationAttribute, 'LIKE', "%{$searchTerm}%");
                            });
                        },
                        function (Builder $query) use ($attribute, $searchTerm) {
                            $query->orWhere($attribute, 'LIKE', "%{$searchTerm}%");
                        }
                    );
                }
            });
            return $this;
        });
        app()->singleton('general_setting', function () {
            if (Cache::has('general_settings')) {
                return Cache::get('general_settings');
            } else {
                foreach (GeneralSetting::get() as $setting) {
                    $datas[$setting->name] = $setting->value;
                }
                Cache::rememberForever('general_settings', function () use($datas) {
                    return $datas;
                });
            }
        });

        app()->singleton('notifications', function () {
            return Notification::latest()->get();
        });

        app()->singleton('permission_list', function() {
            return Role::with(['permissions' => function($query){
                $query->select('permissions.id as per_id','route','module_id','parent_id','role_permission.role_id');
            }])->get(['id','name']);
        });
    }

}
